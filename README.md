### Programmipõhised testid
Задание по *Õppepraktika II*
Проверить [Парадокс Монти Холла](https://ru.wikipedia.org/wiki/%D0%9F%D0%B0%D1%80%D0%B0%D0%B4%D0%BE%D0%BA%D1%81_%D0%9C%D0%BE%D0%BD%D1%82%D0%B8_%D0%A5%D0%BE%D0%BB%D0%BB%D0%B0)
Программа написана на С#  
Использовался **TestTools.UnitTesting** это Unit библиотека для C#  
Тестами покрыто 99% программы.  
Весь код прокомментирован 
  
Список тестов:  
* OpenTheDoorTest  
* TestFindSelectedDoorIndex  
* TestFindCarDoorIndex  
* RandomDoorValuesTest1  
* SelectRandomDoorTest1  
* OpenRandomDoorTest1  
* PlayerWonOneGameTest  
* StartTheGame_PlayerDontChangesSelectedDoorTest  
* ChangeTheSelectedDoorByPlayerTest  
* ChangeTheSelectedDoorByPlayer1000TimesTest  
* StartTheGame_PlayerChangesSelectedDoorTest1  
* StartTheGame_PlayerChangesSelectedDoorTest2