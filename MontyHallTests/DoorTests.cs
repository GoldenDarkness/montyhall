﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MontyHall;
using MontyHall.Player;

namespace MontyHallTests
{
    [TestClass]
    public class DoorTests
    {
        /// <summary>
        ///     Проверка работы метода <see cref="Door.OpenTheDoor"/>
        /// </summary>
        [TestMethod]
        public void OpenTheDoorTest()
        {
            var door = new Door();
            door.OpenTheDoor(); //Отрываем дверь
            Assert.AreEqual(true, door.IsDoorOpen);
        }

        /// <summary>
        ///     Проверка работы метода <see cref="GameOfMontyHall.FindSelectedDoorIndex"/>
        /// </summary>
        [TestMethod]
        public void TestFindSelectedDoorIndex()
        {
            var game = new GameOfMontyHall(100, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            game.Doors[10].IsSelectedByPlayer = true; //Дверь под номер 10, пометим что мы её выбрали
            var index = game.FindSelectedDoorIndex(); //Находим индекс двери, которую выбрали
            Assert.AreEqual(index, 10); //index должен быть 10, если все правильно
        }

        /// <summary>
        ///     Проверка работы метода <see cref="GameOfMontyHall.FindCarIndex"/>
        /// </summary>
        [TestMethod]
        public void TestFindCarDoorIndex()
        {
            var game = new GameOfMontyHall(100, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            game.Doors[10].BehindTheDoor = BehindTheDoor.Car;   //Дверь под номер 10, пометим что там стоит машина
            var index = game.FindCarIndex(); //Находим индекс двери, где стоит машина
            Assert.AreEqual(index, 10); ////index должен быть 10, если все правильно
        }

        [TestMethod]
        public void RandomDoorValuesTest1()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            int carIsBehindTheFirstDoorCounter = 0;
            int carIsBehindTheSecondDoorCounter = 0;
            int carIsBehindTheThirdDoorCounter = 0;

            for (int i = 0; i < 1000; i++)
            {
                game.DoorInit();

                if (game.Doors[0].BehindTheDoor == BehindTheDoor.Car)
                    carIsBehindTheFirstDoorCounter++;
                if (game.Doors[1].BehindTheDoor == BehindTheDoor.Car)
                    carIsBehindTheSecondDoorCounter++;
                if (game.Doors[2].BehindTheDoor == BehindTheDoor.Car)
                    carIsBehindTheThirdDoorCounter++;
            }

            Console.WriteLine("RandomDoorValuesTest1: " +
                              carIsBehindTheFirstDoorCounter + " " +
                              carIsBehindTheSecondDoorCounter + " " +
                              carIsBehindTheThirdDoorCounter + "\n");

            Assert.IsTrue(carIsBehindTheFirstDoorCounter > 300 &&
                          carIsBehindTheSecondDoorCounter > 300 &&
                          carIsBehindTheThirdDoorCounter > 300);
        }

        [TestMethod]
        public void SelectRandomDoorTest1()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            int firstDoorIsSelectedCounter = 0;
            int secondDoorIsSelectedCounter = 0;
            int thirdDoorIsSelectedCounter = 0;

            for (int i = 0; i < 1000; i++)
            {
                game.SelectRandomDoor();
                switch (game.Player.SelectedDoor)
                {
                    case 1:
                        firstDoorIsSelectedCounter++;
                        break;
                    case 2:
                        secondDoorIsSelectedCounter++;
                        break;
                    case 3:
                        thirdDoorIsSelectedCounter++;
                        break;
                }
            }
            Console.WriteLine("SelectRandomDoorTest1: " +
                              firstDoorIsSelectedCounter + " " +
                              secondDoorIsSelectedCounter + " " +
                              thirdDoorIsSelectedCounter + "\n");

            Assert.IsTrue(firstDoorIsSelectedCounter > 300 &&
                          secondDoorIsSelectedCounter > 300 &&
                          thirdDoorIsSelectedCounter > 300);
        }


        [TestMethod]
        public void OpenRandomDoorTest1()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoChangeTheSelectedDoor());
            int firstDoorIsOpen = 0;
            int secondDoorIsOpen = 0;
            int thirdDoorIsOpen = 0;

            for (int i = 0; i < 1000; i++)
            {
                game.DoorInit();
                game.SelectRandomDoor();
                game.OpenRandomDoor();


                if (game.Doors[0].IsDoorOpen == true)
                    firstDoorIsOpen++;
                if (game.Doors[1].IsDoorOpen == true)
                    secondDoorIsOpen++;
                if (game.Doors[2].IsDoorOpen == true)
                    thirdDoorIsOpen++;
            }

            Console.WriteLine("OpenRandomDoorTest1: " + firstDoorIsOpen + " " + secondDoorIsOpen + " " +
                              thirdDoorIsOpen + "\n");

            Assert.IsTrue(firstDoorIsOpen > 300 &&
                          secondDoorIsOpen > 300 &&
                          thirdDoorIsOpen > 300);
        }

        /// <summary>
        ///     Проверяем, что метод <see cref="GameOfMontyHall.PlayerWonTheGame"/> что нам начисляют победу
        /// </summary>
        [TestMethod]
        public void PlayerWonOneGameTest()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            game.DoorInit();

            int indexOfCarDoors = game.FindCarIndex();
            game.Player.SelectedDoor = indexOfCarDoors + 1;
            game.PlayerWonTheGame();
            Assert.AreEqual(1, game.Player.NumberOfGamesWon);
        }

        /// <summary>
        ///     Проверка стратегии, когда игрок не меняет дверь
        /// </summary>
        [TestMethod]
        public void StartTheGame_PlayerDontChangesSelectedDoorTest()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            game.Start();
            Console.WriteLine("NumberOfGamesWon: " + game.Player.NumberOfGamesWon);
            Assert.IsTrue(game.Player.NumberOfGamesWon < 400); //Кол-во выйгранных игр не должно превышать 33% ± 2%
        }

        /// <summary>
        ///     Проверяем работу <see cref="PlayerWhoChangeTheSelectedDoor.ChangeTheSelectedDoorByPlayer"/> что мы меняем дверь
        /// </summary>
        [TestMethod]
        public void ChangeTheSelectedDoorByPlayerTest()
        {
            List<Door> doors = new List<Door>(3);
            var player = new PlayerWhoChangeTheSelectedDoor();
            doors.Add(new Door());
            doors.Add(new Door());
            doors.Add(new Door());
            doors[0].OpenTheDoor();
            player.SelectedDoor = 2;
            player.ChangeTheSelectedDoorByPlayer(doors);

            Assert.AreEqual(3, player.SelectedDoor);
        }

        [TestMethod]
        public void ChangeTheSelectedDoorByPlayer1000TimesTest()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoChangeTheSelectedDoor());

            int firstDoorIsSelectedCounter = 0;
            int secondDoorIsSelectedCounter = 0;
            int thirdDoorIsSelectedCounter = 0;

            for (int i = 0; i < 1000; i++)
            {
                game.DoorInit();
                game.SelectRandomDoor();
                game.OpenRandomDoor();
                game.Player.ChangeTheSelectedDoorByPlayer(game.Doors);

                switch (game.Player.SelectedDoor)
                {
                    case 1:
                        firstDoorIsSelectedCounter++;
                        break;
                    case 2:
                        secondDoorIsSelectedCounter++;
                        break;
                    case 3:
                        thirdDoorIsSelectedCounter++;
                        break;
                }
            }

            Console.WriteLine("SelectRandomDoorTest1: " +
                              firstDoorIsSelectedCounter + " " +
                              secondDoorIsSelectedCounter + " " +
                              thirdDoorIsSelectedCounter + "\n");

            Assert.IsTrue(firstDoorIsSelectedCounter > 300 &&
                          secondDoorIsSelectedCounter > 300 &&
                          thirdDoorIsSelectedCounter > 300);
        }

        /// <summary>
        ///     Проверка стратегии, когда игрок меняет дверь
        /// </summary>
        [TestMethod]
        public void StartTheGame_PlayerChangesSelectedDoorTest1()
        {
            var game = new GameOfMontyHall(3, 1000, new PlayerWhoChangeTheSelectedDoor()); //Инициализация, 3 двери и 1000 итерации. Используем стратегию сменты двери
            game.Start(); //запускаем
            Console.WriteLine("NumberOfGamesWon: " + game.Player.NumberOfGamesWon);
            Assert.IsTrue(game.Player.NumberOfGamesWon > 600); ////Кол-во выйгранных игр должно быть 66% ± 2%
        }

        /// <summary>
        ///     Тоже самое что и <see cref="StartTheGame_PlayerChangesSelectedDoorTest1"/> только с большим кол-во итерации
        /// </summary>
        [TestMethod]
        public void StartTheGame_PlayerChangesSelectedDoorTest2()
        {
            var game = new GameOfMontyHall(3, 10000, new PlayerWhoChangeTheSelectedDoor());
            game.Start();
            Console.WriteLine("NumberOfGamesWon: " + game.Player.NumberOfGamesWon);
            Assert.IsTrue(game.Player.NumberOfGamesWon > 6000);
        }
    }
}