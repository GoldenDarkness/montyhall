﻿using System;
using MontyHall.Player;

namespace MontyHall
{
    class Program
    {
        static void Main(string[] args)
        {
            var game1 = new GameOfMontyHall(3, 1000, new PlayerWhoChangeTheSelectedDoor());
            var game2 = new GameOfMontyHall(3, 1000, new PlayerWhoDontChangeTheSelectedDoor());
            game1.Start();
            game2.Start();
            Console.WriteLine("Player who switched his choice: " + game1.Player.NumberOfGamesWon / 10 + "%");
            Console.WriteLine("Player who didn't switch his choice: " + game2.Player.NumberOfGamesWon / 10 + "%");
            Console.ReadKey();
        }
    }
}
