﻿namespace MontyHall
{
    /// <summary>
    ///     Сущность дверь
    /// </summary>
    public class Door
    {
        /// <summary>
        ///     Конструктор, инициализируем переменные
        /// </summary>
        public Door()
        {
            //Значения по умолчанию
            IsSelectedByPlayer = false;
            IsDoorOpen = false;
            BehindTheDoor = BehindTheDoor.Goat;
        }

        /// <summary>
        ///     Выбрана ли игроком
        /// </summary>
        public bool IsSelectedByPlayer { get; set; }

        /// <summary>
        ///     Открыта ли дверь
        /// </summary>
        public bool IsDoorOpen { get; set; }

        /// <summary>
        ///     То, что находиться за дверью
        /// </summary>
        public BehindTheDoor BehindTheDoor { get; set; }

        /// <summary>
        ///     Открыть дверь
        /// </summary>
        public void OpenTheDoor()
        {
            IsDoorOpen = true;
        }
    }
}
