﻿using System;
using System.Collections.Generic;
using MontyHall.Player;

namespace MontyHall
{
    public class GameOfMontyHall
    {
        readonly Random _rand = new Random();

        /// <summary>
        ///     Кол-во игр
        /// </summary>
        public int NumberOfGames { get; }

        /// <summary>
        ///     Список дверей
        /// </summary>
        public List<Door> Doors { get; }

        /// <summary>
        ///     Игрок
        /// </summary>
        public PlayerOfMontyHall Player { get; }

        /// <summary>
        ///     Конструкток <see cref="GameOfMontyHall"/>. Настраиваем параметры игры.
        /// </summary>
        /// <param name="numberOfDoors">Количество дверей, которая будет в игре</param>
        /// <param name="numberOfGames">Количество игр т.е. итерации</param>
        /// <param name="player">Тип игрока, можеn быть или <see cref="PlayerWhoChangeTheSelectedDoor"/> или <see cref="PlayerWhoChangeTheSelectedDoor"/></param>
        public GameOfMontyHall(int numberOfDoors, int numberOfGames, PlayerOfMontyHall player)
        {
            //Инициализируем перменные
            Doors = new List<Door>(numberOfDoors);
            this.NumberOfGames = numberOfGames;
            this.Player = player;


            for (int i = 0; i < numberOfDoors; i++) //Итерация от 0 до кол-во дверей в numberOfDoors
            {
                Doors.Add(new Door()); //Добавляем дверь в список
            }
        }

        /// <summary>
        ///     Инициализируем двери, выбирает случаеную дверь, где будет машина
        /// </summary>
        public void DoorInit()
        {
            foreach (Door door in Doors)
            {
                door.BehindTheDoor = BehindTheDoor.Goat;
                door.IsDoorOpen = false;
                door.IsSelectedByPlayer = false;
            }

            //Выбираем случаеную дверь, где будет находиться машина
            var index = _rand.Next(0, Doors.Count);
            Doors[index].BehindTheDoor = BehindTheDoor.Car;
        }

        /// <summary>
        ///     выбираем случаеную дверь
        /// </summary>
        public void SelectRandomDoor()
        {
            var index = _rand.Next(0, Doors.Count); //выбираес случаенное число от 0 до кол-во дверей
            Player.SelectedDoor = index+1; //указываем дверь, которую выбрали случаенно
            Doors[index].IsSelectedByPlayer = true;
        }

        /// <summary>
        ///     Открываем случаную дверь
        /// </summary>
        public void OpenRandomDoor()
        {
            int indexOfSelectedDoor = FindSelectedDoorIndex();
            int indexOfCarDoor = FindCarIndex();
            bool doorIsAlreadyOpened = false;

            if (indexOfCarDoor != indexOfSelectedDoor)
            {
                for (int i = 0; i < Doors.Count; i++)
                {
                    if (i != indexOfSelectedDoor && i != indexOfCarDoor)
                    {
                        Doors[i].OpenTheDoor();
                    }
                }
            }
            else
            {
                while (!doorIsAlreadyOpened)
                {
                    for (int i = 0; i < Doors.Count; i++)
                    {
                        if (i != indexOfSelectedDoor && !doorIsAlreadyOpened)
                        {
                            if (_rand.Next(0, 100) == 50)
                            {
                                Doors[i].OpenTheDoor();
                                doorIsAlreadyOpened = true;
                            }

                        }
                    }
                }
            }

        }

        /// <summary>
        ///     Находим дверь, которыю выбрал игрок
        /// </summary>
        /// <returns>Индекс двери, которую выьрал игрок</returns>
        public int FindSelectedDoorIndex()
        {
            int indexOfSelectedDoor = 0;
            for (int i = 0; i < Doors.Count; i++)
            {
                if (Doors[i].IsSelectedByPlayer)
                {
                    indexOfSelectedDoor = i;
                }
            }
            return indexOfSelectedDoor;
        }

        /// <summary>
        ///     Находим дверь, за которой стоит машина
        /// </summary>
        /// <returns>Индекс двери, где стоит машина</returns>
        public int FindCarIndex()
        {
            int indexOfCarDoor = 0;
            for (int i = 0; i < Doors.Count; i++)
            {
                if (Doors[i].BehindTheDoor == BehindTheDoor.Car)
                {
                    indexOfCarDoor = i;
                }
            }
            return indexOfCarDoor;
        }

        public bool PlayerWonTheGame()
        {
            int indexOfCarDoors = FindCarIndex();

            if (Player.SelectedDoor == (indexOfCarDoors + 1)) //Если индекс выбранной двери совпадает с индексом, там где стоит машина
            {
                //Увеличваем счетчик выйгранных игр
                Player.NumberOfGamesWon++;
                return true; //Возвращаем True, что мы выйграли
            }

            return false; //Не выйграли

        }

        /// <summary>
        ///  Начинаем игру
        /// </summary>
        public void Start()
        {
            for (int i = 0; i < NumberOfGames; i++) //Итерация от 0 до кол-во игр NumberOfGames
            {
                DoorInit(); //Инициализируем двери
                SelectRandomDoor(); //Выбираем случаеную дверь
                OpenRandomDoor();   //Открываем случаеную дверь
                Player.ChangeTheSelectedDoorByPlayer(Doors); //Логика смены двери
                PlayerWonTheGame(); //Выйграли
            }
        }
    }
}
