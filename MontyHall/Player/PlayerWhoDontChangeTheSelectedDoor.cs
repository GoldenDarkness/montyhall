﻿using System.Collections.Generic;

namespace MontyHall.Player
{
    /// <summary>
    ///  Игрок, который не будет менять дверь
    /// </summary>
    public class PlayerWhoDontChangeTheSelectedDoor : PlayerOfMontyHall
    {
        public override void ChangeTheSelectedDoorByPlayer(List<Door> doors)
        {
           //Так как, мы не будем менять дверь, то метод останеться пустым.
        }
    }
}
