﻿using System.Collections.Generic;

namespace MontyHall.Player
{
    /// <summary>
    ///     Абстрактный класс сущности Игрок.
    /// </summary>
    public abstract class PlayerOfMontyHall
    {
        /// <summary>
        ///     Констуктор, инициализируем первоначальные значения
        /// </summary>
        protected PlayerOfMontyHall()
        {
            NumberOfGamesWon = 0;
            SelectedDoor = 0;
        }

        /// <summary>
        ///     Количество выйграных игр
        /// </summary>
        public int NumberOfGamesWon { get; set; }

        /// <summary>
        ///     Индекс выбранной двери
        /// </summary>
        public int SelectedDoor { get; set; }

        /// <summary>
        ///     Абстрактный метод, который надо реализовать наследуемым классам. Отвечает за логику/тактику смены дверей
        /// </summary>
        /// <param name="doors">Список дверей</param>
        public abstract void ChangeTheSelectedDoorByPlayer(List<Door> doors);

    }
}
