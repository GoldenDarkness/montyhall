﻿using System.Collections.Generic;

namespace MontyHall.Player
{
    /// <summary>
    ///     Игрок, который будет менять дверь
    /// </summary>
    public class PlayerWhoChangeTheSelectedDoor : PlayerOfMontyHall
    {
        public override void ChangeTheSelectedDoorByPlayer(List<Door> doors)
        {
            bool selectedDoorIsAlreadyChanged = false; //Выбранную двеь ещё не меняли
            for (int i = 0; i < doors.Count; i++) //Итерация от 0 до кол-во дверей
            {
                if (!doors[i].IsDoorOpen && !selectedDoorIsAlreadyChanged) //Если дверь не открыта и не меняли
                {
                    if (SelectedDoor != i + 1) //Нужно делать +1 потому что в doors храняться двери с 0 до какого-то числа, т.е. первая дверь это индекс 0, тогда как первая дверь в SelectedDoor это будет 1.
                    {
                        doors[SelectedDoor - 1].IsSelectedByPlayer = false;
                        SelectedDoor = i + 1;
                        doors[i].IsSelectedByPlayer = true; //Помечаем, что дверь выбранна игроком
                        selectedDoorIsAlreadyChanged = true;
                    }
                }
            }
        }
    }
}
