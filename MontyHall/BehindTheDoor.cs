﻿namespace MontyHall
{
    /// <summary>
    ///     То что можем находиться за дверью
    /// </summary>
    public enum BehindTheDoor
    {
        /// <summary>
        ///     Козел
        /// </summary>
        Goat = 0,

        /// <summary>
        ///     Машина
        /// </summary>
        Car = 1
    }
}
